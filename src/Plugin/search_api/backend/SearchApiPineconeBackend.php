<?php

namespace Drupal\search_api_pinecone\Plugin\search_api\backend;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Json;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\search_api\Backend\BackendPluginBase;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Query\QueryInterface;
use Drupal\search_api\SearchApiException;
use Probots\Pinecone\Client as Pinecone;

/**
 * Indexes items using the Pinecone.io API.
 *
 * @SearchApiBackend(
 *   id = "pinecone",
 *   label = @Translation("Pinecone"),
 *   description = @Translation("Indexes items using the Pinecone API.")
 * )
 */
class SearchApiPineconeBackend extends BackendPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /**
   * Pinecone API client instance.
   *
   * @var \Probots\Pinecone\Client
   */
  protected $pineconeClient;

  /**
   * The Pinecone Index ID.
   *
   * @var string
   */
  protected $pineconeIndexId;

  /**
   * The key repository.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $plugin->keyRepository = $container->get('key.repository');

    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'key' => '',
      'environment' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $pinecone_console_link = Link::fromTextAndUrl('Pinecone console', Url::fromUri('https://app.pinecone.io/'))
      ->toString();

    $form['key'] = [
      '#type' => 'key_select',
      '#title' => $this->t('API key'),
      '#description' => $this->t('To find your Pinecone API key, open the @pinecone_console_link and click API Keys.', [
        '@pinecone_console_link' => $pinecone_console_link,
      ]),
      '#default_value' => $this->configuration['key'],
      '#empty_value' => '',
      '#empty_label' => '',
      '#required' => TRUE,
    ];

    $form['environment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Environment'),
      '#description' => $this->t('To find your environment name, open the @pinecone_console_link and click API Keys.', [
        '@pinecone_console_link' => $pinecone_console_link,
      ]),
      '#default_value' => $this->configuration['environment'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    try {
      if ($pinecode_client = $this->getPineconeClient()) {
        $response = $pinecode_client->index()->list();
      }
    }
    catch (\Exception $e) {
      $message = $this->t('Wrong key or environment: @message', [
        '@message' => $e->getMessage(),
      ]);
      $form_state->setErrorByName('key', $message);
      $form_state->setErrorByName('environment', $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['key'] = $form_state->getValue('key');
    $this->configuration['environment'] = $form_state->getValue('environment');
  }

  /**
   * {@inheritdoc}
   */
  public function viewSettings() {
    $info[] = [
      'label' => $this->t('Pinecone.io API'),
      'info' => Json::encode($this->configuration),
    ];
    return $info;
  }

  /**
   * {@inheritdoc}
   */
  public function addIndex(IndexInterface $index) {
    if ($pinecode_client = $this->getPineconeClient()) {
      $settings = $index->getThirdPartySettings('search_api_pinecone');

      $dimension = $settings['dimension'];
      $metric = $settings['metric'];
      $pods = $settings['pods'];
      $replicas = $settings['replicas'];
      $pod_type = $settings['pod_type'];
      $metadata_config = empty($settings['metadata_config']) ? NULL : $settings['metadata_config'];
      $source_collection = empty($settings['source_collection']) ? NULL : $settings['source_collection'];

      $response = $pinecode_client->index()->create($this->getPineconeIndexId($index->id()), $dimension, $metric, $pods, $replicas, $pod_type, $metadata_config, $source_collection);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function updateIndex(IndexInterface $index) {
    if ($pinecode_client = $this->getPineconeClient()) {
      $settings = $index->getThirdPartySettings('search_api_pinecone');
      if (!empty($settings)) {
        $replicas = $settings['replicas'];
        $pod_type = $settings['pod_type'];

        $response = $pinecode_client->index($this->getPineconeIndexId($index->id()))->configure($pod_type, $replicas);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function removeIndex($index) {
    if (is_object($index)) {
      try {
        if ($pinecode_client = $this->getPineconeClient()) {

          $response = $pinecode_client->index($this->getPineconeIndexId($index->id()))->delete();
        }
      }
      catch (\Exception $e) {
        throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function indexItems(IndexInterface $index, array $items) {
    $indexed = [];

    foreach ($items as $id => $item) {
      try {
        $this->indexItem($item);
        $indexed[] = $id;
      }
      catch (\Exception $e) {
        $this->getLogger()->error($e->getMessage());
      }
    }

    return $indexed;
  }

  /**
   * Indexes a single item on the specified index.
   *
   * @param \Drupal\search_api\Item\ItemInterface $item
   *   The item to index.
   *
   * @throws \Drupal\search_api\SearchApiException
   *   If the Pinecone.io API error occurs.
   */
  protected function indexItem(ItemInterface $item) {
    try {
      if ($pinecode_client = $this->getPineconeClient()) {
        $index = $item->getIndex();
        $vectors = [];
        foreach ($item->getFields() as $field) {
          if ($field->getType() === 'embedding') {
            foreach ($field->getValues() as $delta => $values) {
              $vectors[] = [
                'id' => $item->getId() . '_' . $delta,
                'values' => $values['vector'],
                'metadata' => [
                  'meta' => $values['item'],
                ],
              ];
            }
          }
        }

        $response = $pinecode_client->index($this->getPineconeIndexId($index->id()))->vectors()->upsert($vectors);
      }
    }
    catch (\Exception $e) {
      throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function search(QueryInterface $query) {
    // @todo .
  }

  /**
   * {@inheritdoc}
   */
  public function deleteItems(IndexInterface $index, array $item_ids) {
    try {
      if ($pinecode_client = $this->getPineconeClient()) {
        $response = $pinecone->index($this->getPineconeIndexId($index->id()))->vectors()->delete($item_ids);
      }
    }
    catch (\Exception $e) {
      throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteAllIndexItems(IndexInterface $index, $datasource_id = NULL) {
    try {
      if ($pinecode_client = $this->getPineconeClient()) {
        $response = $pinecone->index($this->getPineconeIndexId($index->id()))->vectors()->delete([], NULL, TRUE);
      }
    }
    catch (\Exception $e) {
      throw new SearchApiException($e->getMessage(), $e->getCode(), $e);
    }
  }

  /**
   * Helper function to obtain the Pinecone API key.
   *
   * @return string
   *   The Pinecone API key.
   */
  protected function getPineconeApiKey() {
    if ($key = $this->configuration['key']) {
      if ($key_object = $this->keyRepository->getKey($key)) {
        return $key_object->getKeyValue();
      }
    }
  }

  /**
   * Helper function to gets the Pinecone client.
   *
   * @return \Probots\Pinecone\Client
   *   The Pinecone client.
   */
  protected function getPineconeClient() {
    if (!isset($this->pineconeClient)) {
      try {
        if ($api_key = $this->getPineconeApiKey()) {
          $environment = $this->configuration['environment'];
          $this->pineconeClient = new Pinecone($api_key, $environment);
        }
      }
      catch (\Exception $e) {
        $message = t('There was a problem connecting to the Pinecone API please check your credentials: @message', [
          '@message' => $e->getMessage(),
        ]);

        throw new \Exception($message);
      }
    }

    return $this->pineconeClient;
  }

  /**
   * Helper function to get the Pinecone Index ID.
   *
   * @param string $index_id
   *   The Search API index ID.
   *
   * @return string
   *   The Pinecone Index ID.
   */
  protected function getPineconeIndexId($index_id) {
    if (!$this->pineconeIndexId) {
      $this->pineconeIndexId = str_replace('_', '-', $index_id);
    }

    return $this->pineconeIndexId;
  }

}
